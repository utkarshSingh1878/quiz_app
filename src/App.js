import React, { useEffect, useState } from "react";
import "./App.css"
import { useSelector, useDispatch } from 'react-redux'
import Welcome from "./components/Welcome"
import QuizCard from "./components/QuizCard"
import { motion } from "framer-motion";

function App() {
  const dispatch = useDispatch();
  const userData = useSelector((state) => state.quizReducer.userData)
  const { name, category, difficulty } = useSelector((state) => state.quizReducer.userData);
  const [questions, setQuestions] = useState([]);
  async function fetchQst() {
    const res = await (await fetch(`https://opentdb.com/api.php?amount=10&category=${category}&difficulty=${difficulty}`)).json()
    setQuestions(res.results)
  }
  useEffect(() => {
    fetchQst()
  }, [])
  console.log(questions)
  return (
    <div className="app">
      {
        (userData.isRegistered && questions.length > 0) ? (
          <motion.div
            initial={{ y: 1000 }}
            animate={{ y: 0 }}
            transition={{ type: "spring", duration: 3 }}
          >
            <QuizCard
              questions={questions}
            // qCount={qCount}
            // setQCount={setQCount}
            />
          </motion.div>
        ) : (
          <>
            <motion.h1
              initial={{ y: -200 }}
              animate={{ y: 0 }}
              transition={{ type: "spring", duration: 1.5 }}
              whileHover={{ scale: 1.1 }}
              style={{ color: "white" }}
            >
              Quiz App
            </motion.h1>
            <Welcome />
          </>
        )
      }
    </div>
  )
}

export default App;