import { createSlice } from '@reduxjs/toolkit'

export const quizSlice = createSlice({
    name: "quizGame",
    initialState: {
        userData: {
            name: "",
            category: "",
            difficulty: "",
            isRegistered: false,
            // name: "harsh",
            // category: "17",
            // difficulty: "medium",
            // isRegistered: true,
            // isRegistered: false,
        },
    },
    reducers: {
        addUser: (state, action) => {
            state.userData.category = action.payload.category
            state.userData.difficulty = action.payload.difficulty
            state.userData.name = action.payload.name
            state.userData.isRegistered = !state.userData.isRegistered
        },
    },
})

export const { addUser } = quizSlice.actions;
export default quizSlice.reducer;
