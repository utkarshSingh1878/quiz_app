import { configureStore } from "@reduxjs/toolkit";
import quizReducer from "../redux/quizSlice"
import counterReducer from "../redux/counterSlice"
import scoreReducer from "../redux/scoreSlice"
export const store = configureStore({
    reducer: {
        quizReducer: quizReducer,
        counter: counterReducer,
        score: scoreReducer,
    },
})