import { Button, MenuItem, TextField } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { addUser } from '../redux/quizSlice';
import "./Welcome.css";
import IMG from "../assets/img3.png"
import { motion } from "framer-motion";


function Welcome() {
    const dispatch = useDispatch();
    const [name, setName] = useState("");
    const [diff, setDiff] = useState("");
    const [category, setCategory] = useState([])
    const [catChoice, setCatChoice] = useState("")
    // const [formErr, setFormErr] = useState(false);
    const difficulties = [
        "Easy", "Medium", "Hard"
    ]
    // if ((!/[^a-zA-Z]/.test(name))) {
    //     setFormErr(false)
    // }
    // else {
    //     setFormErr(true)
    // }
    async function fetchCategories() {
        const res = await (await fetch("https://opentdb.com/api_category.php")).json();
        setCategory(res.trivia_categories)
    }
    useEffect(() => {
        fetchCategories()
    }, []);
    function updateUser(e) {
        e.preventDefault();
        if (!(!/[^a-zA-Z ]/.test(name))) {
            alert("Input must be valid")
        } else {
            dispatch(addUser({
                name: name,
                category: catChoice,
                difficulty: diff,
            }))
        }
    }
    // console.log(diff);
    // console.log(catChoice);
    return (
        <div className='welcome'>
            <div className="welcomeLeft">
                <img src={IMG} alt="" style={{ objectFit: "cover", height: "100%", borderRadius: " 18px 0 0 18px" }} />
            </div>
            <div className="welcomeRight">
                <form className="userForm" onSubmit={updateUser}>
                    <TextField label="Name" variant="outlined" onChange={(e) => setName(e.target.value)} required fullWidth color={(!/[^a-zA-Z ]/.test(name)) ? "success" : "warning"} />
                    <TextField
                        select
                        label="Select difficulty"
                        value={diff}
                        onChange={(e) => setDiff(e.target.value)}
                        helperText="Please select your difficulty level"
                        required
                        fullWidth
                    >
                        {difficulties.map((option, idx) => (
                            <MenuItem key={idx} value={option}>
                                {option}
                            </MenuItem>
                        ))}
                    </TextField>
                    <TextField
                        select
                        label="Select category"
                        value={catChoice}
                        onChange={(e) => setCatChoice(e.target.value)}
                        helperText="Please select a category"
                        required
                        fullWidth
                    >
                        {category.map((option, idx) => (
                            <MenuItem key={option.id} value={option.id}>
                                {option.name}
                            </MenuItem>
                        ))}
                    </TextField>
                    <Button variant="contained" type="submit" style={{ marginLeft: "50%" }}  >Goto Quiz</Button>
                </form>
            </div>
        </div>
    )
}
export default Welcome