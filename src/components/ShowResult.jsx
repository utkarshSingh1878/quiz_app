import { Button } from '@mui/material';
import React, { useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { addUser } from '../redux/quizSlice';
import { reset } from "../redux/counterSlice"
import "./ShowResult.css";
function ShowResult() {
    const dispatch = useDispatch();
    const { name, category, difficulty } = useSelector((state) => state.quizReducer.userData);
    const [date, setDate] = useState(new Date())
    console.log("date", date)
    const sCount = useSelector((state) => state.score.value)
    function updateUser(e) {
        e.preventDefault();
        dispatch(addUser({
            name: "",
            category: "",
            difficulty: "",
        }))
        dispatch(reset())
    }
    return (
        <div className="showResult" >
            <h2 className="date">
                {date.toLocaleDateString()}
            </h2>
            <div className="userDetails">
                <p>Name: {name}</p>
                <p>Level: {difficulty}</p>
                <p>{`Score: ${sCount}/10`}</p>
            </div>
            <Button variant="contained" onClick={updateUser} style={{ marginLeft: "50%", marginBottom: "20%" }} >Restart</Button>
        </div>
    )
}

export default ShowResult