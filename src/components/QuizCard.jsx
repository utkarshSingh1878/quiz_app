import { Button } from '@mui/material';
import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { increment } from '../redux/counterSlice';
import { incrementScore } from '../redux/scoreSlice';
import Card from './Card';
import "./QuizCard.css"
import ShowResult from './ShowResult';

function QuizCard({ questions }) {
    const qCount = useSelector((state) => state.counter.value)
    const sCount = useSelector((state) => state.score.value)
    const [currQ, setCurrQ] = useState(questions[qCount]);
    const [userChoice, setUserChoice] = useState("")
    const dispatch = useDispatch()
    // console.log(currQ)
    function handleSubmit(e) {
        e.preventDefault();
        if (userChoice == currQ.correct_answer) {
            dispatch(incrementScore())
        }
        // console.log("your choice", userChoice)
        // console.log("right answer", currQ.correct_answer);
        dispatch(increment())
        // console.log(sCount)
        setUserChoice("")
    }
    useEffect(() => {
        if (qCount <= 9) {
            setCurrQ(questions[qCount])
        }
    }, [qCount])
    return (
        <div className='quizCard' >
            {
                ((qCount <= 9) ?
                    (
                        <>
                            <div className="cardLeft">
                                <h2>Q{qCount + 1}. {`${currQ.question}`}</h2>
                            </div>
                            <div className="cardRight">
                                <form onSubmit={handleSubmit} style={{ height: "100%", margin: "60px" }} >
                                    <div>
                                        <div >
                                            <div className="" onChange={(e) => setUserChoice(e.target.value)}  >
                                                <input type="radio" name="subscribe" value={currQ.correct_answer} />
                                                <label style={{ marginLeft: "10px" }}  >{currQ.correct_answer}</label>
                                                {currQ.incorrect_answers.map((ele, idx) => (
                                                    <div style={{ dispaly: "flex", flexDirection: "column", flexWrap: "wrap" }} >
                                                        <input type="radio" name="subscribe" value={ele} required />
                                                        <label style={{ marginLeft: "10px" }} >{ele}</label>
                                                    </div>
                                                ))

                                                }

                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <Button variant="contained" type="submit" style={{ marginLeft: "50%", fontSize: "12px", fontWeight: "bold", margin: "40px" }} size="small">Next</Button>
                                    </div>
                                </form>
                            </div>
                        </>
                    ) : (
                        <ShowResult />
                    )

                )
            }
        </div>
    )
}

export default QuizCard
