import React from "react";
import * as RDC from 'react-dom/client';
import { Provider } from 'react-redux'
import App from './App';
import { store } from "./redux/store";
import "./index.css";

const root = document.getElementById('root');
const app = RDC.createRoot(root);
app.render(
  <Provider store={store}>
    <App />
  </Provider>
)